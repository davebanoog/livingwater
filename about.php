<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Livingwater System Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="css/imageeffect.css" rel="stylesheet">
</head>

<body>

    <div class="brand">Livingwater Refilling Station</div>
    <div class="address-bar">Keep it healthy, keep it clean.</div>

    <?php
        include 'navbar.php';
        include 'php/initPrice.php';
    ?>

    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">LIVINGWATER
                        <strong>Providing healthy drinking water since 2016.</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-md-6">
                    <img class="img-responsive img-border-left" src="img/logo.png" alt="">
                </div>
                <div class="col-md-6">
                   <p>Located in Metro MNL, Philippines, Livingwater Water Refilling Station serves nearest area for all of your water purification and water bottling needs.</p>
                    <p>Livingwater Water Refilling Station is a family owned business, and has been in business since June 2016.Our goal is to provide our customers with personalized service, the best purified water by reverse osmosis around (purified on-site), and a variety of bottles and other water accessories. </p>
                    
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center"> <strong> Products and Services </strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="hovereffect">
                        <img class="img-responsive" src="img/a.png" alt="">
                        <div class="overlay">
                           <h2>Php<?php echo $aw;?>/gallon</h2>
                           <a class="info" href="faqs.php?id=1">Order</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="hovereffect">
                        <img class="img-responsive" src="img/p.png" alt="">
                        <div class="overlay">
                           <h2>Php<?php echo $pw;?>/gallon</h2>
                           <a class="info" href="faqs.php?id=2">Order</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 text-center">
                    <div class="hovereffect">
                        <img class="img-responsive" src="img/m.png" alt="">
                        <div class="overlay">
                           <h2>Php<?php echo $mw;?>/gallon</h2>
                           <a class="info" href="faqs.php?id=3">Order</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Livingwater System Inc., 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
