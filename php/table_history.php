<?php
	include 'connection.php';
?>
<style>
	.r {
    	font-size: 85%;
	}
</style>
<table>
  <tr>
    <th>Customer Name</th>
    <th>Contact</th>
    <th>Address</th>
	<th>Order</th>
  </tr>

 	<?php 
 		$sql='SELECT * FROM orders WHERE status = 1';

 		$result = $conn->query($sql);
			$num = mysqli_num_rows($result);
			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					$type = getProductName($row['type']);
					$id = $row['orderid'];
					$quantity = $row['quantity'];
					$message = $row['message'];
          $total = computeTotal($row['type'], $row['quantity']);
          $date = $row['orderdate'];
          $ddate = $row['deliverydate'];
 	?>

  <tr>
    <td><?php echo $row['name'];?></td>
    <td><?php echo $row['contactnumber'];?></td>
    <td><?php echo $row['address'];?></td>
    <td>
    <button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#ord<?php echo $id;?>">Order # <?php echo $id;?></button>
    <div class="r">
        <div id="ord<?php echo $id;?>" class="collapse">
        <p>Order date: <strong><?php echo date_format(date_create_from_format('Y-m-d', $date), 'F d, Y');?></strong></p>
        <p>Delivery date: <strong> <?php echo date_format(date_create_from_format('Y-m-d', $ddate), 'F d, Y');?></strong></p>
        <p>Product name: <strong><?php echo $type;?></strong></p>
        <p>Quantity: <strong><?php echo $quantity;?></strong></p>
        <p>Total: <strong><?php echo $total;?></strong></p>
        <p>Message: <strong><?php echo $message;?></strong></p>
        </div>
    </div>
    </td>
  </tr>

  <?php 
  				}
  			}
  ?>
 
 
</table>
<br><br><br><br><br><br><br>