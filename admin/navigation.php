    <!-- Navigation -->
    <?php include '../php/message_counter.php';?>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
        	
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- navbar-brand is hidden on larger screens, but visible when the menu is collapsed -->
                <a class="navbar-brand" href="watah.html">Livingwater</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
           <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                   <li>
						<a href="orders.php">Orders</a>
                    </li>
					<li>
						<a href="pricing.php">Pricing</a>
                    </li>
					<li>
						<a href="feedback.php">User Feedback <span class="badge"><?php if($notif_message!=0){echo $notif_message;}?></span></a>
                    </li>
                    <li>
                        <a href="settings.php">Settings</a>
                    </li>
					<li>
						<a href="../php/logout.php">Logout</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>