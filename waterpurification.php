<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Livingwater System Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="brand">Livingwater Refilling Station</div>
    <div class="address-bar">Keep it healthy, keep it clean.</div>

    <?php
        include 'navbar.php';
    ?>

    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">REVERSE OSMOSIS
                        <strong>WATER PURIFICATION PROCESS</strong>
                    </h2>
                    <hr>
					<p class="intro-text text-center"> Livingwater's purification unit is a seven stage system using reverse osmosis.</p>
                </div>
                <div class="col-lg-12 text-center">
                    <img class="img-responsive img-border img-full" src="img/water_filter.jpg" alt="">
                    <h2>STEP 1:
                        <br>
                        <small>Ag Pre-Filtration</small>
                    </h2>
                    <p>Removes particulate matter down to 20 microns.</p>
    
                    <hr>
                </div>
                <div class="col-lg-12 text-center">
                    <img class="img-responsive img-border img-full" src="img/max.jpg" alt="">
                    <h2>STEP 2:
                        <br>
                        <small>Maxi-cure</small>
                    </h2>
                    <p>An activated carbon filter that removes chlorine, chloramines, solvents, lead, insecticides, gasses, fluoride, pharmceuticals, and other harmful contaminants. Works as a water softener without the use of salt by drawing hardness particles together and crystalizing them. Includes an automatic backwashing system.

					</p>
                    <hr>
                </div>
                <div class="col-lg-12 text-center">
                    <img class="img-responsive img-border img-full" src="img/kadiribes.jpg" alt="">
                    <h2>STEP 3:
                        <br>
                        <small>Micron-Pre Filter</small>
                    </h2>
                    <p>Removes particles, sediment, and algae.</p>
                    <hr>
                </div>
								
				 <div class="col-lg-12 text-center">
                    <img class="img-responsive img-border img-full" src="img/5.jpg" alt="">
                    <h2>STEP 4:
                        <br>
                        <small>Reverse Osmosis Membrane</small>
                    </h2>
                    <p>Water is forced through semipermeable, .002 micron membrane, allowing only pure water molecules to pass.</p>
    <hr>
                    </div>		
					 <div class="col-lg-12 text-center">
                    <img class="img-responsive img-border img-full" src="img/blackaf.jpg" alt="">
                    <h2>STEP 5:
                        <br>
                        <small>Post Carbon-Filtration</small>
                    </h2>
                    <p>Water travels through 1 cubic foot Post Carbon filter with automatic backwash, polishing it for exceptionally smooth taste.</p>
					<hr>
					</div>
					
					 <div class="col-lg-12 text-center">
                    <img class="img-responsive img-border img-full" src="img/uv.jpg" alt="">
                    <h2>STEP 6:
                        <br>
                        <small>Ultraviolet Sterilizer</small>
                    </h2>
                    <p>25 G.PM. stainless steel sterilizer eliminates up to 99.9% of all bacteria.</p>
						<hr>
                    </div>
					
					<div class="col-lg-12 text-center">
                    <img class="img-responsive img-border img-full" src="img/ozone.jpg" alt="">
                    <h2>STEP 7:
                        <br>
                        <small>Ozone </small>
                    </h2>
                    <p>Added to storage tanks to prevent future bacterial growth.</p>
						<hr>
                    </div>
					

            </div>
        </div>

    </div>
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Livingwater System Inc., 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
