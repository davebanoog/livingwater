<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Livingwater System Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div class="brand">Livingwater Refilling Station</div>
    <div class="address-bar">Keep it healthy, keep it clean.</div>

    <?php
        include 'navbar.php';
        include 'php/initSettings.php';
        if(isset($_GET['id'])){
            $type = $_GET['id'];
        }else{
            header ('Location: about.php');
        }
    ?>

    <div class="container">

    <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Order Form 
                        <strong>for delivery.</strong>
                    </h2>
                    <hr>
                    <?php
                        if(isset($_GET['s'])){
                            if($_GET['s']=='success'){
                                echo '<div class="alert alert-success">
                                <strong>Success!</strong> Your order has been placed.
                                </div>';
                            }else if ($_GET['s']=='error'){
                                echo '<div class="alert alert-danger">
                                <strong>Error!</strong> Please complete the form to continue.
                                </div>';
                            }
                        }

                        if(isset($_GET['error'])){
                            echo '<div class="alert alert-danger">
                                <strong>Error!</strong> Please complete the form to continue.
                                </div>';
                        }
                    ?>
                    <form action="php/addOrder.php" method="POST">
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label>Name*</label>
                                <input type="text" class="form-control" name="name">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Contact Number*</label>
                                <input type="tel" class="form-control" name="cn">
                            </div>
                            <div class="form-group col-lg-4">
                                <label>Address* (Complete address)</label>
                                <input type="text" class="form-control" name="addr">
                            </div>
                            
                             <div class="form-group col-lg-4">
                                <label>Email Address</label>
                                <input type="text" class="form-control" name="email">
                            </div>
                            
                            <div class="form-group col-lg-4">
                                <label>Number of gallons</label><br>
                                <select name="quantity">
                                <option value="1"selected>1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                </select>
                            </div>


                            
                                 <input type="hidden" value="<?php echo $_GET['id'];?>" name="type">
                            <div class="clearfix"></div>
                            <div class="form-group col-lg-12">
                                <label>Message</label>
                                <textarea class="form-control" rows="6" name="message"></textarea>
                            </div>
                            <div class="form-group col-lg-12">
                                <input type="hidden" name="save" value="contact">
                                * Required fields.<br>
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                        </form>
                   
                </div>
            </div>
        </div>

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Where is the
                        <strong>Physical Store located?</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-md-8">
                    <!-- Embedded Google Map using an iframe - to select your location find it on Google maps and paste the link as the iframe src. If you want to use the Google Maps API instead then have at it! -->
                    <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1929.5535316133034!2d121.06250365789784!3d14.706536997433773!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTTCsDQyJzIzLjUiTiAxMjHCsDAzJzQ5LjAiRQ!5e0!3m2!1sen!2sph!4v1475140474989" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->

                    <iframe src = "https://maps.google.com/maps?q=<?php echo $lat;?>,<?php echo $long;?>&hl=es;z=14&amp;output=embed" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-4">
                    <p>Phone:
                        <strong><?php echo $phone;?></strong>
                    </p>
                    <p>Email:
                        <strong><a href="mailto:name@example.com"><?php echo $email;?></a></strong>
                    </p>
                    <p>Address:
                        <strong><?php echo $address;?></strong>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        

    </div>
	
	 
                            
                            <div class
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Livingwater System Inc., 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
