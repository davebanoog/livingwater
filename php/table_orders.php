<?php
	include 'connection.php';
?>
<style>
	.r {
    	font-size: 85%;
	}
</style>
<table>
  <tr>
    <th>Customer Name</th>
    <th>Contact</th>
    <th>Address</th>
	<th>Order</th>
  </tr>

 	<?php 
 		$sql='SELECT * FROM orders WHERE status = 0';

 		$result = $conn->query($sql);
			$num = mysqli_num_rows($result);
			if ($result->num_rows > 0) {
				while($row = $result->fetch_assoc()) {
					$type = getProductName($row['type']);
					$id = $row['orderid'];
					$quantity = $row['quantity'];
					$message = $row['message'];
          $total = computeTotal($row['type'], $row['quantity']);
          $date = $row['orderdate'];
 	?>

  <tr>
    <td><?php echo $row['name'];?></td>
    <td><?php echo $row['contactnumber'];?></td>
    <td><?php echo $row['address'];?></td>
    <td>
    <button class="btn btn-primary btn-block" data-toggle="collapse" data-target="#ord<?php echo $id;?>">Order # <?php echo $id;?></button>
    <div class="r">
        <div id="ord<?php echo $id;?>" class="collapse">
        <p>Order date: <strong><?php echo date_format(date_create_from_format('Y-m-d', $date), 'F d, Y');?></strong></p>
        <p>Product name: <strong><?php echo $type;?></strong></p>
        <p>Quantity: <strong><?php echo $quantity;?></strong></p>
        <p>Total: <strong><?php echo $total;?></strong></p>
        <p>Message: <strong><?php echo $message;?></strong></p>
        <form action="../php/delivered.php" method="POST">
          <input type="hidden" name="id" value="<?php echo $id;?>">
          <button type="submit" class="btn btn-primary btn-block">Deliver</button>
        </form>
        </div>
    </div>
    </td>
  </tr>

  <?php 
  				}
  			}
  ?>
 
 
</table>

<?php
	function getProductName($num){
		$name="";
		if($num==1){
			$name = "Alkaline";
		}else if($num==2){
			$name = "Purified";
		}else if($num==3){
			$name = "Mineral";
		}
		return $name;
	}

  function computeTotal($type, $quantity){
      $cost=0;
      if($type==1){
        $cost=45;
      }else if($type==2){
        $cost=35;
      }else if($type==3){
        $cost=25;
      }
      
      $total=$cost*$quantity;

      return $total;
  }
	
?>
<br><br><br><br><br><br><br>