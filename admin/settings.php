<?php session_start();?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Livingwater System Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>

    <!--<div class="brand">Livingwater Refilling Station</div>
    <div class="address-bar">Keep it healthy, keep it clean.</div>-->

    <?php
      include '../php/initSettings.php';
        if(isset($_SESSION['id'])){
            include 'navigation.php';
        }else{
            header('Location: ../../login.php');
        }
    ?>

    <?php 
        include '../php/connection.php';
    ?>

    <div class="container">

         <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Store
                        <strong>Settings</strong>
                    </h2>
                    <hr>
                </div>

                <div class="col-lg-6 col-lg-offset-3">

                    <div class="well">
                        <h4>Password</h4>
                        <?php 
                          if(isset($_GET['s']) AND $_GET['s']=='success'){
                              echo '<div class="alert alert-success">
                                <strong>Success!</strong> Password has been updated.
                                </div>';
                          }else if(isset($_GET['s']) AND $_GET['s']=='error1'){
                              echo '<div class="alert alert-danger">
                                <strong>Error!</strong> Password you enetered did not match.
                                </div>';
                          }else if(isset($_GET['s']) AND $_GET['s']=='error'){
                            echo '<div class="alert alert-danger">
                                <strong>Error!</strong> There has been a problem with the connection.
                                </div>';
                          }
                        ?>
                        <form class="form-horizontal" action="../php/updatePassword.php" method="POST">
                          <div class="form-group">
                            <label class="col-sm-2" for="password">New password:</label>
                            <div class="col-sm-10">
                              <input type="password" class="form-control" name="pw">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2" for="password">Re-type password:</label>
                            <div class="col-sm-10">
                              <input type="password" class="form-control" name="pw1">
                            </div>
                          </div>
                          <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Update</button>
                            </div>
                          </div>
                        </form>
                    </div>
                    <br>
                   <div class="well">
                        <h4 class="text-center">Physical Store</h4>
                        <?php
                          if(isset($_GET['s']) AND $_GET['s']=='success2'){
                              echo '<div class="alert alert-success">
                                <strong>Success!</strong> Settings has been updated.
                                </div>';
                          }else if(isset($_GET['s']) AND $_GET['s']=='error2'){
                              echo '<div class="alert alert-danger">
                                <strong>Error!</strong> There has been an error with the connection.
                                </div>';
                          }
                        ?>
                        <form class="form-horizontal" action="../php/updateSettings.php" method="POST">
                          <div class="form-group">
                            <label class="col-sm-2" for="phone">Phone: </label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="phone" value="<?php echo $phone;?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2" for="email">Email: </label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="email" value="<?php echo $email;?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2" for="address">Address: </label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="addr" value="<?php echo $address;?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2" for="address">Latitude: </label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="lat" value="<?php echo $lat;?>">
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2" for="address">Longitude: </label>
                            <div class="col-sm-10">
                              <input type="text" class="form-control" name="long" value="<?php echo $long;?>">
                            </div>
                          </div>
                          <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                              <button type="submit" class="btn btn-default">Update</button>
                            </div>
                          </div>
                        </form>
                    </div>
                <br><br><br>
                </div>
                <div class="clearfix"></div>

            </div>

        </div>
    </div>


		
                        
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Livingwater System Inc., 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
