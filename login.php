<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Livingwater System Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <?php
        include 'php/connection.php';
            echo '';

        if(isset($_POST['un'])){
            $un = $_POST['un'];

            $pwd = $_POST['pwd'];

            $sql = "SELECT adminid, password FROM admin WHERE username LIKE '".$un."';";
            
            $result = $conn->query($sql);
            $num = mysqli_num_rows($result);
            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    if ($row['password'] == $pwd){
                        $_SESSION['id'] = $row['adminid'];
                        header("Location: admin/orders.php");
                    }else{
                        header("Location: login.php?error=1");
                    }
                }
            }else{
                header("Location: login.php?error=1");
            }
        }
    ?>

    <div class="brand">Livingwater Refilling Station</div>
    <div class="address-bar">Keep it healthy, keep it clean.</div>

    <?php
        include 'navbar.php';
    ?>

    <div class="container">

        <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">ADMIN
                        <strong>Login</strong>
                    </h2>
                    <hr>
                </div>
                <div class="col-lg-4 col-lg-offset-4">
                    <?php         
                        if(isset($_GET['error'])){
                        echo '<div class="alert alert-danger">
                          <strong>Error!</strong> Unauthorized access is strictly prohibited.
                        </div>';
                    }?>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="POST">
                        <div class="form-group">
                        <input type="text" class="form-control" name="un" id="un" placeholder="Username">
                        </div>
                        <div class="form-group">
                        <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Password">
                      </div>
                      <br><br>
                      <button type="submit" class="btn btn-primary btn-block">Submit</button>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Livingwater System Inc., 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
