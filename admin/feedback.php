<?php session_start();?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Livingwater System Inc.</title>

    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/business-casual.css" rel="stylesheet">

    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>

    <!--<div class="brand">Livingwater Refilling Station</div>
    <div class="address-bar">Keep it healthy, keep it clean.</div>-->

    <?php
        if(isset($_SESSION['id'])){
            include 'navigation.php';
        }else{
            header('Location: ../../login.php');
        }
    ?>
    <style>

    #email {
        -moz-border-radius: 0px;
        -webkit-border-radius: 0px;
        border-radius: 0px;
    }

    </style>
    <div class="container">

         <div class="row">
            <div class="box">
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        <strong>Inbox</strong>
                    </h2>
                    <hr>
                </div>
                <?php
                    include '../php/connection.php';

                    $sql = 'SELECT csid, email, subject, state FROM comments_suggestions';

                    $result = $conn->query($sql);
                    $num = mysqli_num_rows($result);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            

                            echo '<a href="view_message.php?id='.$row['csid'].'">
                                    <button id="email" class="btn btn-default btn-block">
                                    <span class="pull-left">';
                            if($row['state']==0){
                                echo '<strong>';

                            }
                            echo $row['email'].': '.$row['subject'];

                            if($row['state']==0){
                                echo '</strong>';
                                echo '&nbsp;<span class="label label-default">New</span>';
                            }

                            echo '</span></button></a>';
                        }
                    }
                ?>
                    
                <br><br><br><br><br><br><br><br><br><br>
                </div>
                <div class="clearfix"></div>

            </div>

        </div>


		
                        
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Copyright &copy; Livingwater System Inc., 2016</p>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
