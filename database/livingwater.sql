-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2016 at 05:26 PM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `livingwater`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminid` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminid`, `username`, `password`) VALUES
(1, 'admin', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `comments_suggestions`
--

CREATE TABLE `comments_suggestions` (
  `csid` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `contactnumber` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `subject` varchar(250) NOT NULL,
  `concern` text NOT NULL,
  `state` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments_suggestions`
--

INSERT INTO `comments_suggestions` (`csid`, `name`, `contactnumber`, `email`, `subject`, `concern`, `state`) VALUES
(1, 'Dave', '09971191826', 'davebanoog@gmail.com', 'No subject', 'Nice site.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `orderid` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `contactnumber` varchar(250) NOT NULL,
  `address` text NOT NULL,
  `email` varchar(250) DEFAULT NULL,
  `orderdate` date NOT NULL,
  `quantity` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `message` text,
  `status` int(11) NOT NULL,
  `deliverydate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`orderid`, `name`, `contactnumber`, `address`, `email`, `orderdate`, `quantity`, `type`, `message`, `status`, `deliverydate`) VALUES
(1, 'Rein Labios', '12345678910', 'Philippines', 'rlabios@gmail.com', '2016-09-20', 1, 1, 'Urgent!', 1, '2016-09-29'),
(6, 'Leo Garong', '09187855357', 'Philippines', 'lgarong@gmail.com', '2016-09-29', 1, 3, '', 1, '2016-09-29'),
(7, 'nholvs', '09463885730', 'san mateo rizal', 'nholvslanderio18@gmail.com', '2016-09-29', 1, 1, 'TY', 1, '2016-09-29'),
(8, 'Jamaela Projas', '09222222222', 'San Mateo Rizal', '', '2016-09-29', 8, 1, 'pls', 1, '2016-09-29');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productid` int(11) NOT NULL,
  `product` varchar(150) NOT NULL,
  `price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productid`, `product`, `price`) VALUES
(2, 'Alkaline Water', 45),
(3, 'Purified Water', 35),
(4, 'Mineral Water', 25);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminid`);

--
-- Indexes for table `comments_suggestions`
--
ALTER TABLE `comments_suggestions`
  ADD PRIMARY KEY (`csid`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`orderid`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comments_suggestions`
--
ALTER TABLE `comments_suggestions`
  MODIFY `csid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `orderid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
